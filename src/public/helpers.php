<?php

// Helper method to determine if a request is valid
function validateHmac($params, $secret) {
  $hmac = $params['hmac'];
  unset($params['hmac']);
  ksort($params);
  $computedHmac = hash_hmac('sha256', http_build_query($params), $secret);

  return hash_equals($hmac, $computedHmac);
}

// Helper method for exchanging credentials
function getAccessToken($shop, $apiKey, $secret, $code) {
  $query = array(
  	'client_id' => $apiKey,
  	'client_secret' => $secret,
  	'code' => $code
  );

  // Build access token URL
  $access_token_url = "https://{$shop}/admin/oauth/access_token";

  // Configure curl client and execute request
  $curl = curl_init();
  $curlOptions = array(
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_URL => $access_token_url,
    CURLOPT_POSTFIELDS => http_build_query($query)
  );
  curl_setopt_array($curl, $curlOptions);
  $jsonResponse = json_decode(curl_exec($curl), TRUE);
  curl_close($curl);

  return $jsonResponse['access_token'];
}

// Helper method for making Shopify API requests
function performShopifyRequest($shop, $token, $resource, $params = array(), $method = 'GET') {
  $url = "https://{$shop}/admin/{$resource}.json";

  $curlOptions = array(
    CURLOPT_RETURNTRANSFER => TRUE
  );

  if ($method == 'GET') {
    if (!is_null($params)) {
      $url = $url . "?" . http_build_query($params);
    }
  } else {
    $curlOptions[CURLOPT_CUSTOMREQUEST] = $method;
  }

  $curlOptions[CURLOPT_URL] = $url;

  $requestHeaders = array(
    "X-Shopify-Access-Token: ${token}",
    "Accept: application/json"
  );

  if ($method == 'POST' || $method == 'PUT') {
    $requestHeaders[] = "Content-Type: application/json";

    if (!is_null($params)) {
      $curlOptions[CURLOPT_POSTFIELDS] = json_encode($params);
    }
  }

  $curlOptions[CURLOPT_HTTPHEADER] = $requestHeaders;

  $curl = curl_init();
  curl_setopt_array($curl, $curlOptions);
  $response = curl_exec($curl);
  curl_close($curl);

  return json_decode($response, TRUE);
}

// Helper method for verify hook
function verifyWebhook($data, $hmac_header, $secret)
{
  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $secret, true));
  return hash_equals($hmac_header, $calculated_hmac);
}

// Helper method for check Out of stock tag rule
function checkOutOfStock($shop, $token, $productToModify, $db)
{
	// check collection
	$needTag = 0;
	$query = performShopifyRequest( // get product links to collections one-to-many
		$shop, $token, 'collects', ['product_id' => $productToModify['id']]
	);
	if(!empty($query['collects'])){
		foreach($query['collects'] as $collect){ // look each collections
			$taggedCollection = $db->query("select * from collections where id={$collect['collection_id']}")->fetch(PDO::FETCH_OBJ);
			if(!empty($taggedCollection->id)){ // check if product collection enabled
				$needTag = 1;
			}
		}
	}
	
	$oldTags = explode(',', $productToModify['tags']);
	// count of variants with inventory_quantity <= 0 
	$countVariantsOutOfStock = 0;
	// count of variants with inventory_management
	$countManagedVariants = 0;
	foreach($productToModify['variants'] as $variant){
		if($variant['inventory_management'] == 'shopify') {
			$countManagedVariants++;
			if($variant['inventory_quantity'] <= 0){
				$countVariantsOutOfStock++;
			}
		}
	}
	
	// all managed variants out of stock, need tag
	if($countVariantsOutOfStock == $countManagedVariants && $needTag) {
		// dont have tag, need to add it
		if(array_search('Out of stock', $oldTags) === false){ 
			$oldTags[] = 'Out of stock';
			$query = performShopifyRequest(
				$shop, $token, 'products/'.$productToModify['id'], ['product' => ['id' => $productToModify['id'], 'tags' =>  implode (", ", $oldTags)]], 'PUT'
			);
		}
	}
	// one or more variants not out of stock, dont't need tag
	else { 
		// already have tag, need to remove it
		if(($key = array_search('Out of stock', $oldTags)) !== false){ 
			unset($oldTags[$key]);
			$query = performShopifyRequest(
				$shop, $token, 'products/'.$productToModify['id'], ['product' => ['id' => $productToModify['id'], 'tags' =>  implode (", ", $oldTags)]], 'PUT'
			);
		}
	}	
}

