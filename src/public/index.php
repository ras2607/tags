<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require 'helpers.php';
$dotenv = new Dotenv\Dotenv(__DIR__ . str_repeat(DIRECTORY_SEPARATOR . '..', 2));
$dotenv->load();

$config = [
  'apiKey' => $_ENV['API_KEY'],
  'secret' => $_ENV['SECRET'],
  'host' => $_ENV['HOST'],
  'db_host' => $_ENV['DB_HOST'],
  'db_name' => $_ENV['DB_NAME'],
  'db_user' => $_ENV['DB_USER'],
  'db_pass' => $_ENV['DB_PASS']
];

$app = new \Slim\App($config);

// init database
$container = $app->getContainer();
$container['db'] = function ($app) {
    $pdo = new PDO('mysql:host=' . $app->get('db_host') . ';dbname=' . $app->get('db_name'),
        $app->get('db_user'), $app->get('db_pass'));
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// install route
$app->get('/', function (Request $request, Response $response) {
	$host = $this->get('host');
	$shop = $request->getQueryParam('shop');
	if(empty($shop))
		die('Empty domain');
	$token =  $this->db->query("select * from shops where shop='{$shop}'")->fetch(PDO::FETCH_OBJ);
	
	// run app
	if($token) 
		return $response->withRedirect($this->router->pathFor('configs')."?shop=".$shop);
	// get token before
	else { 
		$redirectUri = $host . $this->router->pathFor('oauth');
		$installUrl = "https://{$shop}/admin/oauth/authorize?client_id={$this->get('apiKey')}&scope=write_products&redirect_uri={$redirectUri}";
		return $response->withRedirect($installUrl);
	}
});

// callback oauth route
$app->get('/oauth', function (Request $request, Response $response) {
	$params = $request->getQueryParams();
	$shop = $params['shop'];
	$validHmac = validateHmac($params, $this->get('secret'));

	if ($validHmac) {
		// get token from shopify
		$token = getAccessToken($shop, $this->get('apiKey'), $this->get('secret'), $params['code']); 
		// save token for site in DB
		$this->db->prepare("insert into shops (shop,token) values ('{$shop}', '{$token}')")->execute(); 
		return $response->withRedirect($this->router->pathFor('configs')."?shop=".$shop);
	} else {
		return $response->getBody()->write("This request is NOT from Shopify!");
	}
})->setName('oauth');

// configs route to change collection settings
$app->get('/configs', function (Request $request, Response $response) {
	$params = $request->getQueryParams();
	$shop = $params['shop'];
	$shopParams = $this->db->query("select * from shops where shop='{$shop}'")->fetch(PDO::FETCH_OBJ);
	$token =  $shopParams->token;
	
	$responseBody = "<form style='width: 100%;' action='{$this->get('host')}/configs' method='POST'>";
	
	// save signature for shop's webhooks
	$responseBody .= "<h3>Add webhooks:</h3>";
	$responseBody .= "<b>Inventory level update</b> to route <b>{$this->get('host')}/hook_level</b><br>";
	$responseBody .= "<b>Product update</b> to route <b>{$this->get('host')}/hook_product</b>";
	$responseBody .= "<h3>Enter webhook's signature from /admin/settings/notifications:</h3>";
	$responseBody .= "<input style='width:50%;' type='text' name='hook' value='{$shopParams->hook}'><br>";

	// form for collections to enable rules
	$responseBody .= "<h3>Select collections for this App:</h3>";
	
	$responseBody .= "<input type='hidden' name='shop' value='{$shop}'>";
	$query = performShopifyRequest(
		$shop, $token, 'custom_collections'
	);
	$collections = $query['custom_collections'];

	// echo all shop's collections
	foreach ($collections as $collection){
		// check if collection's rule on
		$taggedCollection = $this->db->query("select * from collections where id={$collection['id']}")->fetch(PDO::FETCH_OBJ);
		if(!empty($taggedCollection->id)) 
			$tagged = 'checked';
		else $tagged = '';
		$responseBody .= "<p><input type='checkbox' name='collections[]' {$tagged} value='{$collection['id']}'> {$collection['title']}</p>";
	}
	$responseBody .= "<button type='submit'>Save</button>";
	$responseBody .= "</form>";
	// flash message
	if(!empty($params['s']))
		$responseBody .= "<h4>Saved</h4>";
	
	return $response->getBody()->write($responseBody);
})->setName('configs');

//save configs route
$app->post('/configs', function (Request $request, Response $response) {
	$params = $request->getParsedBody();
	$shop = $params['shop'];
	$this->db->query("delete from collections")->execute();
	
	// save collection that have enabled rule to DB
	foreach($params['collections'] as $collection_id){
		$this->db->prepare("insert into collections values ({$collection_id})")->execute(); 
	}
	
	// save webhook's signature for shop
	$this->db->prepare("update shops set hook = '{$params['hook']}' where shop='{$params['shop']}'")->execute();
	return $response->withRedirect($this->router->pathFor('configs')."?s=1&shop=".$shop);
});

// route for product update hook
$app->post('/hook_product', function (Request $request, Response $response) { 
	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
	
	// verify webhook by shop token stored in DB
	$shopParams = $this->db->query("select * from shops where shop='{$shop}'")->fetch(PDO::FETCH_OBJ);
	$verified = verifyWebhook($data, $hmac_header, $shopParams->hook);
	if(!empty($verified)){
		$token =  $shopParams->token;
		$data = json_decode($data, true);
		$needTag = 0;		
		// get product links to collections one-to-many
		$query = performShopifyRequest( 
			$shop, $token, 'collects', ['product_id' => $data['id']]
		);
		if(!empty($query['collects'])){
			// look each collections
			foreach($query['collects'] as $collect){ 
				$taggedCollection = $this->db->query("select * from collections where id={$collect['collection_id']}")->fetch(PDO::FETCH_OBJ);
				// check if product collection enabled
				if(!empty($taggedCollection->id)){ 
					foreach($data['variants'] as $variant){
						// check if price for sale
						if($variant['price'] < $variant['compare_at_price']){ 
							$needTag = 1;
							break;break;
						}
					}
				}
			}
		}
		$oldTags = explode(',', $data['tags']);
		// product need tag Sale
		if($needTag){ 
			// dont have tag, need to add it
			if(array_search('Sale', $oldTags) === false){ 
				$oldTags[] = 'Sale';
				$query = performShopifyRequest(
					$shop, $token, 'products/'.$data['id'], ['product' => ['id' => $data['id'], 'tags' =>  implode (", ", $oldTags)]], 'PUT'
				);
			}
		}
		// product don't need tag Sale
		else{ 
			// already have tag, need to remove it
			if(($key = array_search('Sale', $oldTags)) !== false){ 
				unset($oldTags[$key]);
				$query = performShopifyRequest(
					$shop, $token, 'products/'.$data['id'], ['product' => ['id' => $data['id'], 'tags' =>  implode (", ", $oldTags)]], 'PUT'
				);
			}
		}
		// chech out of stock
		checkOutOfStock($shop, $token, $data, $this->db);
	}
});

// route for inventory level update hook
// need when event "Inventory level update" triggers without "Product update" event
// this happends for example when inventory_quantity changes after successfull order, not from admin panel
$app->post('/hook_level', function (Request $request, Response $response) { 
	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
	
	// verify webhook by shop token stored in DB
	$shopParams = $this->db->query("select * from shops where shop='{$shop}'")->fetch(PDO::FETCH_OBJ);
	$verified = verifyWebhook($data, $hmac_header, $shopParams->hook);
	if(!empty($verified)){
		$shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
		$token =  $this->db->query("select * from shops where shop='{$shop}'")->fetch(PDO::FETCH_OBJ)->token;
		$data = json_decode($data, true);
		
		// have inventory_item_id, we need product_id to add tag
		$inventory_item_id = $data['inventory_item_id']; 
		
		// search updated products at same time as inventory_level
		$products = performShopifyRequest(  
			$shop, $token, 'products', ['updated_at_min' => $data['updated_at'], 'updated_at_max' => $data['updated_at']]
		);
		
		// search from products and its variants inventory_item_id
		foreach($products['products'] as $product){ 
			foreach($product['variants'] as $variant){ 
				if($variant['inventory_item_id'] == $inventory_item_id){ 
					// get product
					$productToModify = $product;
				}
			}
		}
		if(!empty($productToModify)){
			checkOutOfStock($shop, $token, $productToModify, $this->db);
		}
	}
});

$app->run();
